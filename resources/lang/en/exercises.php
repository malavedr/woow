<?php

return [
    'home' => 'Home',
    'list' => 'Exercises list',
    'calculate' => 'Calculate',
    'result' => 'Ways to climb Ladder: :steps',
    'input' => [
        'steps' => 'Steps',
    ],
    'name' => [
        'climb-ladder' => 'Ways to climb Ladder',
        'product-restocking' => 'Product restocking',
    ],
    'description' => [
        'climb-ladder' => 'You are climbing a ladder that has n steps. In each step you can choose to go up 1 step or go up 2. Program a solution that, given a staircase of n steps, finds in how many different ways it can be climbed to reach the end.',
        'product-restocking' => 'You will have to write a program that reads the JSon file, where the purchases of a customer are located and calculates the date of possible buyback of the products that he bought (only those that he bought at least 2 times).',
    ],
    'messages' => [
        'steps-null' => 'You must enter the number of steps!',
        'purchases_null' => 'No purchases found!',
        'purchases_file_null' => 'Purchase file not found!',
        'buyback_null' => 'The customer has not made a buyback!'
    ],
    'placeholder' => [
        'steps' => 'Stair steps'
    ],
    'label' => [
        'steps' => 'Stair steps',
        'product_name' => 'Product',
        'times_bought' => 'Times bought',
        'estimated_frequency' => 'Estimated frequency (Days)',
        'last_purchase' => 'Last purchase',
        'next_purchase' => 'Next purchase',
    ]
];
@extends('welcome')

@section('title')
    {{ trans('exercises.name.product-restocking') }}
@endsection

@section('content')
<div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
    <div class="grid grid-cols-1">
        <div class="p-6">
            @if (empty($products->message))
                <table class="table-auto">
                    <thead>
                        <tr>
                            <th class="px-4 py-2 text-left">{{ trans('exercises.label.product_name') }}</th>
                            <th class="px-4 py-2">{{ trans('exercises.label.times_bought') }}</th>
                            <th class="px-4 py-2">{{ trans('exercises.label.estimated_frequency') }}</th>
                            <th class="px-4 py-2">{{ trans('exercises.label.last_purchase') }}</th>
                            <th class="px-4 py-2">{{ trans('exercises.label.next_purchase') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products->products_history as $product)
                            <tr>
                                <td class="border px-4 py-2">{{ $product['name'] }}</td>
                                <td class="border px-4 py-2 text-center">{{ count($product['buybacks']) }}</td>
                                <td class="border px-4 py-2 text-center">{{ $product['average'] }}</td>
                                <td class="border px-4 py-2 text-center">{{ $product['date'] }}</td>
                                <td class="border px-4 py-2 text-center">{{ $product['estimated-date-purchase'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h1 class="text-center font-bold text-red-600 text-2xl">{{ $products->message }}</h1>
            @endif
        </div>
    </div>
</div>
@endsection
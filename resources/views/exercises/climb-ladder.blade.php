@extends('welcome')

@section('title')
    {{ trans('exercises.name.climb-ladder') }}
@endsection

@section('content')
<div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
    <div class="grid grid-cols-1">
        <div class="p-6">
            <div class="flex items-center">
                <div class="ml-4 text-lg leading-7 font-semibold">
                    <h2>{{ trans('exercises.name.climb-ladder') }}</h2>
        
                    <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                        {{ trans('exercises.description.climb-ladder') }}
                    </div>
                    
                    <br>
                    
                    <form method="GET" action="{{ route('exercise.climb-ladder') }}">
                        @csrf
                    
                        <label for="steps">{{ trans('exercises.input.steps') }}:</label>
                        <input id="steps" name="steps" type="text" placeholder="{{ trans('exercises.placeholder.steps') }}" value="{{ isset($request['steps']) ? $request['steps'] : '' }}" class="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                        
                        <input type="submit" value="{{ trans('exercises.calculate') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                        @error('steps') <div class="text-red-600">{{ $message }}</div> @enderror
                    </form>

                    @if (!empty($steps))
                        <h1 class="text-green-700 font-bold">{{ trans('exercises.result', ['steps' => $steps]) }}</h1>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
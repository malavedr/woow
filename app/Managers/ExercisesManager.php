<?php

namespace App\Managers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class ExercisesManager
{
    /**
     * Determine the number of ways there are to climb a ladder
     * with 1 or 2 steps at a time.
     *
     * @param integer $steps
     * @return integer
     */
    public function getWaysToClimbLadder(int $steps): int
    {
        if ($steps < 2) {
            return 1;
        }

        $a = $b = $c = 1;

        for ($i = 2; $i <= $steps; $i++) {
            $c = $b;
            $b = $a + $b;
            $a = $c;
        }

        return $b;
    }

    /**
     * Returns the storage instance for the exercise files.
     *
     * @return Storage instance
     */
    public function getExercisesDisk()
    {
        return Storage::disk('public');
    }

    /**
     * Returns a customer's buyback list
     * with the estimate of the next purchase for each product.
     *
     * @return StdClass message|products_history
     * message: If there is an error message|null;
     * products_history: Customer buyback list|null.
     */
    public function getProductRestocking()
    {
        $response = new \StdClass();
        $response->message = null;
        $response->products_history = [];
        $purchases_file_name = config('filesystems.purchases.file_name');

        $storage = $this->getExercisesDisk();

        if ($storage->exists($purchases_file_name)) {
            $contents = $storage->get($purchases_file_name);
            $purchases = json_decode($contents, true);

            if (!isset($purchases['customer']['purchases']) || count($purchases['customer']['purchases']) == 0) {
                $response->message = trans('exercises.messages.purchases_null');

                return $response;
            }

            //I navigate $purchases['customer']['purchases'] grouping purchases of the same product.
            foreach ($purchases['customer']['purchases'] as $purchase) {
                foreach ($purchase['products'] as $product) {
                    if (!isset($response->products_history[$product['sku']])) {
                        $response->products_history[$product['sku']] = [
                            'name' => $product['name'],
                            'date' => $purchase['date'],
                            'buybacks' => null,
                        ];
                    } else {
                        $new_date_purchese = Carbon::parse($purchase['date']);
                        $old_date_purchese = Carbon::parse($response->products_history[$product['sku']]['date']);

                        $response->products_history[$product['sku']]['date'] = $purchase['date'];
                        $response->products_history[$product['sku']]['buybacks'][] = $new_date_purchese->diffInDays($old_date_purchese);
                    }
                }
            }

            //I navigate $ response-> products_history eliminating purchases without repurchase 
            //and calculating the median between purchase days.
            foreach ($response->products_history as $sku => $product) {
                if (empty($product['buybacks'])) {
                    unset($response->products_history[$sku]);
                    continue;
                }

                sort($response->products_history[$sku]['buybacks']);

                $response->products_history[$sku]['average'] = $this->getAverageDays($response->products_history[$sku]['buybacks']);
                $response->products_history[$sku]['estimated-date-purchase'] = Carbon::parse($response->products_history[$sku]['date'])
                    ->addDays($response->products_history[$sku]['average'])->format('Y-m-d');
            }

            if (count($response->products_history) == 0) {
                $response->message = trans('exercises.messages.buyback_null');
            }

            return $response;
        }

        $response->message = trans('exercises.messages.purchases_file_null');

        return $response;
    }

    /**
     * Returns the median between the days of repurchase of a product.
     *
     * @param array $days
     * @return integer
     */
    private function getAverageDays(array $days): int
    {
        $number_periods = intval((count($days) - 1) / 2);

        if (count($days) % 2 != 0) {
            $average = (count($days) > 1) ? 
                ($days[$number_periods - 1] + $days[$number_periods] + $days[$number_periods + 1]) / 3 : 
                $days[$number_periods];
        } else {
            $average = ($days[$number_periods] + $days[$number_periods + 1]) / 2;
        }

        return $average;
    }
}

<?php

namespace App\Http\Controllers;

use App\Managers\ExercisesManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ExercisesController extends Controller
{
    private $manager;

    /**
     * An instance of ExercisesManager is initialized
     * which takes care of the logic of the Excercises.
     *
     * @param ExercisesManager $manager
     */
    public function __construct(ExercisesManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * List of proposed exercises
     *
     * @return void
     */
    public function list()
    {
        return view('exercises.list');
    }

    /**
     * It allows you to determine the number
     * of ways in which you can climb stairs
     * climbing 1 or 2 steps at a time.
     *
     * @param Request $request
     * @return void
     */
    public function getWaysToClimbLadder(Request $request)
    {
        $steps = null;
        $validator = Validator::make($request->all(), [
            'steps' => 'nullable|numeric|min:1'
        ]);

        if ($validator->fails()) {
            return redirect(route('exercise.climb-ladder'))
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->has('steps') && empty($request->get('steps'))) {
            $validator->errors()->add('steps', trans('exercises.messages.steps-null'));

            return redirect(route('exercise.climb-ladder'))
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->has('steps') && $request->get('steps') > 0) {
            $steps = $this->manager->getWaysToClimbLadder($request->get('steps'));
        }

        $request = $request->all();

        return view('exercises.climb-ladder', compact('steps', 'request'));
    }

    /**
     * Displays the list of repurchased products
     * with their possible future repurchase date
     *
     * @return void
     */
    public function getProductRestocking()
    {
        $products = [];
        $purchases_file_name = config('filesystems.purchases.file_name');
        $products = $this->manager->getProductRestocking();

        return view('exercises.product-restocking', compact('products'));
    }
}

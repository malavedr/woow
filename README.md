# README #

The steps to follow to properly configure the repository are set out below.

### Requirements ###

* PHP 7.4.1 or higher
* Node v14.17.1 or higher

### How do I get set up? ###

* git clone https://malavedr@bitbucket.org/malavedr/woow.git
* composer install
* nmp install
* npm run dev
* php artisan test
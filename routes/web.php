<?php

use App\Http\Controllers\ExercisesController;
use Illuminate\Support\Facades\Route;

//List of exercises
Route::get('/', [ExercisesController::class, 'list'])->name('home');

//Calculate the different ways to climb a ladder, taking 1 or 2 steps
Route::get('/ways-to-climb-ladder', [ExercisesController::class, 'getWaysToClimbLadder'])->name('exercise.climb-ladder');

//Estimate the time of purchase of a product, taking from a .json the last purchases of the client
Route::get('/product-restocking', [ExercisesController::class, 'getProductRestocking'])->name('exercise.product-restocking');
<?php

namespace Tests\Feature\Exercises;

use Tests\TestCase;
use Illuminate\Support\Facades\Session;
use App\Managers\ExercisesManager;

class ClimbLadderTest extends TestCase
{
    /**
     * If the home is visited, it must return status 200.
     *
     * @return void
     */
    public function test_list_exercises_status()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * If the home is visited, the title of the exercise Ways to climb Ladder should be displayed.
     *
     * @return void
     */
    public function test_list_exercises_climb_ladder_title()
    {
        $response = $this->get('/');

        $response->assertSee(trans('exercises.name.climb-ladder'));
    }

    /**
     * If the home is visited, the description of the exercise Ways to climb Ladder should be displayed.
     *
     * @return void
     */
    public function test_list_exercises_climb_ladder_description()
    {
        $response = $this->get('/');

        $response->assertSee(trans('exercises.description.climb-ladder'));
    }

    /**
     * If the home is visited, the route of the exercise Ways to climb Ladder should be created.
     *
     * @return void
     */
    public function test_list_exercises_climb_ladder_route()
    {
        $response = $this->get('/');

        $response->assertSee(route('exercise.climb-ladder'));
    }

    /**
     * If the home is visited, the title of the exercise Product restocking should be displayed.
     *
     * @return void
     */
    public function test_list_exercises_product_restocking_title()
    {
        $response = $this->get('/');

        $response->assertSee(trans('exercises.name.product-restocking'));
    }

    /**
     * If the home is visited, the description of the exercise Product restocking should be displayed.
     *
     * @return void
     */
    public function test_list_exercises_product_restocking_description()
    {
        $response = $this->get('/');

        $response->assertSee(trans('exercises.description.product-restocking'));
    }

    /**
     * If the home is visited, the route of the exercise Product restocking should be created.
     *
     * @return void
     */
    public function test_list_exercises_product_restocking_route()
    {
        $response = $this->get('/');

        $response->assertSee(route('exercise.product-restocking'));
    }

    /**
     * If the Ways to climb Ladder exercise is visited, it must return status 200.
     *
     * @return void
     */
    public function test_exercises_climb_ladder_status()
    {
        $response = $this->get('/ways-to-climb-ladder');

        $response->assertStatus(200);
    }

    /**
     * If the Ways to climb Ladder exercise is visited, the title of the exercise should be displayed.
     *
     * @return void
     */
    public function test_exercises_climb_ladder_title()
    {
        $response = $this->get('/ways-to-climb-ladder');

        $response->assertSee(trans('exercises.name.climb-ladder'));
    }

    /**
     * If the Ways to climb Ladder exercise is visited, the description of the exercise should be displayed.
     *
     * @return void
     */
    public function test_exercises_climb_ladder_description()
    {
        $response = $this->get('/ways-to-climb-ladder');

        $response->assertSee(trans('exercises.description.climb-ladder'));
    }

    /**
     * If the Ways to climb Ladder exercise is visited, a form must be created with the following characteristics.
     *
     * @return void
     */
    public function test_exercises_climb_ladder_form_submit()
    {
        $response = $this->get('/ways-to-climb-ladder');
        $form = '<form method="GET" action="'.route('exercise.climb-ladder').'">';

        $response->assertSee($form, false);
    }

    /**
     * If the Ways to climb Ladder exercise is visited, a input hidden must be created with the following characteristics.
     *
     * @return void
     */
    public function test_exercises_climb_ladder_input_token()
    {
        $response = $this->get('/ways-to-climb-ladder');
        $label = '<input type="hidden" name="_token" value="'.Session::token().'">';

        $response->assertSee($label, false);
    }

    /**
     * If the Ways to climb Ladder exercise is visited, a label must be created with the following characteristics.
     *
     * @return void
     */
    public function test_exercises_climb_ladder_label_steps()
    {
        $response = $this->get('/ways-to-climb-ladder');
        $label = '<label for="steps">'.trans('exercises.input.steps').':</label>';

        $response->assertSee($label, false);
    }

    /**
     * If the Ways to climb Ladder exercise is visited, a input text must be created with the following characteristics.
     *
     * @return void
     */
    public function test_exercises_climb_ladder_input_steps()
    {
        $response = $this->get('/ways-to-climb-ladder');
        $input = '<input id="steps" name="steps" type="text" placeholder="'.trans('exercises.label.steps').'" value="" class="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">';

        $response->assertSee($input, false);
    }

    /**
     * If the Ways to climb Ladder exercise is visited, a input button must be created with the following characteristics.
     *
     * @return void
     */
    public function test_exercises_climb_ladder_button_calculate()
    {
        $response = $this->get('/ways-to-climb-ladder');
        $button = '<input type="submit" value="'.trans('exercises.calculate').'" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">';

        $response->assertSee($button, false);
    }

    /**
     * When a number of steps is not provided, it is redirected to the exercise route.
     *
     * @return void
     */
    public function test_exercises_climb_ladder_validate_redirect()
    {
        $response = $this->get('/ways-to-climb-ladder?steps=');

        $response->assertStatus(302);
        $response->assertSessionHasErrors(['steps' => trans('exercises.messages.steps-null')]);
    }

    /**
     * When an invalid number of steps is provided, it returns a message where a numeric is required.
     *
     * @return void
     */
    public function test_exercises_climb_ladder_validate_steps_invalid()
    {
        $response = $this->get('/ways-to-climb-ladder?steps=Yeppa');

        $response->assertSessionHasErrors(['steps' => trans('validation.numeric', ['attribute' => 'steps'])]);
    }

    /**
     * When the number 0 is provided, it returns a message where a higher numeric is required.
     *
     * @return void
     */
    public function test_exercises_climb_ladder_validate_steps_zero()
    {
        $response = $this->get('/ways-to-climb-ladder?steps=0');

        $response->assertSessionHasErrors(['steps' => trans('validation.min.numeric', ['attribute' => 'steps', 'min' => 1])]);
    }

    /**
     * When a valid number is provided, the number of ways the ladder can be climbed is displayed.
     *
     * @return void
     */
    public function test_exercises_climb_ladder_validate_valid()
    {
        $steps = rand(1, 20);
        $response = $this->get('/ways-to-climb-ladder?steps='.$steps);

        $manager = new ExercisesManager();
        $result = $manager->getWaysToClimbLadder($steps);

        $response->assertSessionHasNoErrors();
        $response->assertSee(trans('exercises.result', ['steps' => $result]));
    }
}

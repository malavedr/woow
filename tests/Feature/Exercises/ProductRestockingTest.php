<?php

namespace Tests\Feature\Exercises;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Managers\ExercisesManager;

class ProductRestockingTest extends TestCase
{
    /**
     * If the Product restocking exercise is visited, it must return status 200.
     *
     * @return void
     */
    public function test_exercises_product_restocking_status()
    {
        $response = $this->get('/product-restocking');

        $response->assertStatus(200);
    }

    /**
     * When there is no .json file for purchases, a message is returned indicating it
     *
     * @return void
     */
    public function test_exercises_product_restocking_purchase_file_does_not_exist()
    {
        config(['filesystems.purchases.file_name' => 'invalid']);
        $response = $this->get('/product-restocking');

        $response->assertSee(trans('exercises.messages.purchases_file_null'));
    }

    /**
     * When in the .json file of purchases, there is no customer data, a message is returned indicating it.
     *
     * @return void
     */
    public function test_exercises_product_restocking_purchase_file_custumer_null()
    {
        config(['filesystems.purchases.file_name' => 'purchases-v2-customer-null.json']);
        $response = $this->get('/product-restocking');

        $response->assertSee(trans('exercises.messages.purchases_null'));
    }

    /**
     * When in the .json file of purchases, there is no purchases data, a message is returned indicating it.
     *
     * @return void
     */
    public function test_exercises_product_restocking_purchase_file_purcheses_null()
    {
        config(['filesystems.purchases.file_name' => 'purchases-v2-purcheses-null.json']);
        $response = $this->get('/product-restocking');

        $response->assertSee(trans('exercises.messages.purchases_null'));
    }

    /**
     * When in the .json file of purchases, there are no repurchased products, a message is returned indicating it.
     *
     * @return void
     */
    public function test_exercises_product_restocking_purchase_file_not_buyback()
    {
        config(['filesystems.purchases.file_name' => 'purchases-v2-buyback.json']);
        $response = $this->get('/product-restocking');

        $response->assertSee(trans('exercises.messages.buyback_null'));
    }

    /**
     * If the Product restocking exercise is visited, the following columns should be displayed.
     *
     * @return void
     */
    public function test_exercises_product_restocking_labels()
    {
        $response = $this->get('/product-restocking');

        $response->assertSee(trans('exercises.label.product_name'));
        $response->assertSee(trans('exercises.label.times_bought'));
        $response->assertSee(trans('exercises.label.estimated_frequency'));
        $response->assertSee(trans('exercises.label.last_purchase'));
        $response->assertSee(trans('exercises.label.next_purchase'));
    }

    /**
     * If the Product restocking exercise is visited, a table with the buyback estimate should be displayed.
     *
     * @return void
     */
    public function test_exercises_product_restocking_buyback_valid()
    {
        $response = $this->get('/product-restocking');

        $manager = new ExercisesManager();
        $products_buyback = $manager->getProductRestocking();

        foreach ($products_buyback->products_history as $product) {
            $response->assertSee('<td class="border px-4 py-2">'.$product['name'].'</td>', false);
            $response->assertSee('<td class="border px-4 py-2 text-center">'.count($product['buybacks']).'</td>', false);
            $response->assertSee('<td class="border px-4 py-2 text-center">'.$product['average'].'</td>', false);
            $response->assertSee('<td class="border px-4 py-2 text-center">'.$product['date'].'</td>', false);
            $response->assertSee('<td class="border px-4 py-2 text-center">'.$product['estimated-date-purchase'].'</td>', false);
        }
    }
}
